package com.jonny.dbtools.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class FormController {


    @FXML
    public TextField ignoreTableName;
    @FXML
    public TextField ignorePrefix;
    @FXML
    public TextField ignoreSuffix;
    @FXML
    protected ChoiceBox dbType;
    @FXML
    protected ChoiceBox docType;
    @FXML
    protected TextField username;
    @FXML
    protected PasswordField password;
    @FXML
    protected TextField port;
    @FXML
    protected TextField dbName;
    @FXML
    protected TextField host;
    @FXML
    protected Label schemaLabel;
    @FXML
    protected TextField schema;

}
