package com.jonny.dbtools.controller

import com.jonny.dbtools.app.MainApplication
import com.jonny.dbtools.components.Alert
import com.jonny.dbtools.constant.DBType
import com.jonny.dbtools.domain.DatabaseDTO
import com.jonny.dbtools.service.DbService
import com.jonny.dbtools.service.impl.DbServiceImpl
import javafx.application.Platform
import javafx.beans.value.ChangeListener
import javafx.beans.value.ObservableValue
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.layout.VBox
import javafx.scene.web.WebView
import javafx.stage.Stage

class MainController implements Initializable {

    private DbService dbService

    @FXML
    private VBox mainBox;

    MainController() {
        this.dbService = new DbServiceImpl()
    }

    @FXML
    private FormController formController;


    @FXML
    protected void export() {

        DatabaseDTO databaseDTO = new DatabaseDTO()
        databaseDTO.setUsername(formController.username?.getText())
        databaseDTO.setPassword(formController.password?.getText())
        databaseDTO.setPort((formController.port?.getText() ?: 0) as Integer)
        databaseDTO.setDbName(formController.dbName?.getText())
        databaseDTO.setHost(formController.host?.getText())
        databaseDTO.setDocType(formController.docType?.getValue() as String)
        databaseDTO.setSchema(formController.schema?.getText())
        databaseDTO.setDbTpe(formController.dbType?.getValue() as String)
        databaseDTO.setIgnoreTableName(formController.ignoreTableName?.getText() as String)
        databaseDTO.setIgnorePrefix(formController.ignorePrefix?.getText() as String)
        databaseDTO.setIgnoreSuffix(formController.ignoreSuffix?.getText() as String)

        try {
            dbService.export(databaseDTO)
        } catch (Exception e) {
            Alert.error(e.getMessage())
            e.printStackTrace()
        }
    }

    /**
     *  tabPanel的change事件
     */
    void bindEvent() {
        formController.dbType.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                int selectedIndex = newValue.intValue();
                if (selectedIndex == 0) {
                    visible(false);
                    initDefault(DBType.MYSQL);
                } else if (selectedIndex == 1) {
                    visible(false);
                    initDefault(DBType.ORACLE);
                } else {
                    visible(true);
                    initDefault(DBType.POSTGRES);
                }
            }
        });
    }

    private void visible(boolean flag) {
        formController.schemaLabel.setVisible(flag)
        formController.schema.setVisible(flag)
    }

    @Override
    void initialize(URL location, ResourceBundle resources) {
        bindEvent();
        initDefault(DBType.MYSQL);
    }

    /**
     *  初始化默认值
     * @param dbType
     */
    void initDefault(String dbType) {
        formController.host.setText(formController.host?.text ?: "localhost")
        formController.password.setText("")
        formController.ignorePrefix.setText("")
        formController.ignoreTableName.setText("")
        formController.ignoreSuffix.setText("")
        switch (dbType) {
            case DBType.MYSQL:
                formController.dbName.setText("mysql")
                formController.username.setText("root")
                formController.port.setText("3306")
                break;
            case DBType.ORACLE:
                formController.dbName.setText("orcl")
                formController.username.setText("system")
                formController.port.setText("1521")
                break;
            case DBType.POSTGRES:
                formController.dbName.setText("postgres")
                formController.username.setText("postgres")
                formController.port.setText("5432")
                formController.schema.setText("public")
                break;
            default:
                break;
        }
    }

    void plusClick() {
        Platform.runLater(new Runnable() {
            @Override
            void run() {
                new MainApplication().start(new Stage())
            }
        })
        ((Stage) mainBox.getScene().getWindow()).close()
    }

    void about() {
        String html = "<div style='font-family: 微软雅黑'><h3 style='text-align:center'>数据库表结构导出工具</h3>\n<p>version: 1.0</p>\n" +
                "<p>Release date: 2022.1</p>" +
                "<p>说明: db-tools是一款javafx + screw开发的数据库表结构文档导出工具</p>\n" +
                "<p>支持的数据库: mysql、oracle、postgres</p>\n" +
                "<p>导出文档类型: html/markdown/word</p>" +
                "<p>软件地址: <a href=\"https://gitee.com/gntlee/db-tools\">https://gitee.com/gntlee/db-tools</a></p></div>";
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.INFORMATION)
        alert.setTitle("关于")
        alert.setHeaderText("")
        WebView webView = new WebView();
        webView.getEngine().loadContent(html)
        webView.setPrefSize(500, 300)
        alert.setResizable(true);
        alert.getDialogPane().setContent(webView)
        alert.showAndWait()
    }
}
