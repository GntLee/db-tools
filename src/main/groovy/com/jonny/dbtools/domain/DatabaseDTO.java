package com.jonny.dbtools.domain;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class DatabaseDTO {

    @NotBlank(message = "用户名不能为空")
    private String username;

    @NotBlank(message = "密码不能为空")
    private String password;

    @NotNull(message = "端口不能为空")
    @Min(value = 0, message = "端口不正确")
    private Integer port;

    private String dbTpe;

    @NotBlank(message = "数据库名不能为空")
    private String dbName;

    @NotBlank(message = "主机ip不能为空")
    private String host;

    private String docType;

    private String schema;

    private String ignoreTableName;

    private String ignorePrefix;

    private String ignoreSuffix;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDbTpe() {
        return dbTpe;
    }

    public void setDbTpe(String dbTpe) {
        this.dbTpe = dbTpe;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getIgnoreTableName() {
        return ignoreTableName;
    }

    public void setIgnoreTableName(String ignoreTableName) {
        this.ignoreTableName = ignoreTableName;
    }

    public String getIgnorePrefix() {
        return ignorePrefix;
    }

    public void setIgnorePrefix(String ignorePrefix) {
        this.ignorePrefix = ignorePrefix;
    }

    public String getIgnoreSuffix() {
        return ignoreSuffix;
    }

    public void setIgnoreSuffix(String ignoreSuffix) {
        this.ignoreSuffix = ignoreSuffix;
    }
}
