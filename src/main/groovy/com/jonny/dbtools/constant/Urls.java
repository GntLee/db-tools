package com.jonny.dbtools.constant;

import java.util.HashMap;
import java.util.Map;

public interface Urls {

    Map<String, String> URLS = new HashMap<String, String>() {{
        put("mysql", "jdbc:mysql://{0}:{1}/{2}?useSSL=false&allowPublicKeyRetrieval=true&characterEncoding=UTF8&serverTimezone=Asia/Shanghai");
        put("postgres", "jdbc:postgresql://{0}:{1}/{2}");
        put("oracle", "jdbc:oracle:thin:@{0}:{1}:{2}");
    }};
}
