package com.jonny.dbtools.constant;

public interface DBType {

    String MYSQL = "mysql";

    String ORACLE = "oracle";

    String POSTGRES = "postgres";
}
