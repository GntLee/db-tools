package com.jonny.dbtools.constant;

import java.util.HashMap;
import java.util.Map;

public interface Type {

    Map<String, String> DRIVERS = new HashMap<String, String>() {{
        put("mysql", "com.mysql.jdbc.Driver");
        put("postgres", "org.postgresql.Driver");
        put("oracle", "oracle.jdbc.OracleDriver");
    }};
}
