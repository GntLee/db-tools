package com.jonny.dbtools.constant;

import cn.smallbun.screw.core.engine.EngineFileType;

import java.util.HashMap;
import java.util.Map;

public interface Doc {

    Map<String, EngineFileType> DOCS = new HashMap<String, EngineFileType>() {
        {
            put("html", EngineFileType.HTML);
            put("markdown", EngineFileType.MD);
            put("word", EngineFileType.WORD);
        }
    };
}
