package com.jonny.dbtools.service

import com.jonny.dbtools.domain.DatabaseDTO

interface DbService {

    void export(DatabaseDTO databaseDTO);
}
