package com.jonny.dbtools.components;

public class Alert<T> {

    public static <T> void error(T msg) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.ERROR);
        alert.setContentText(String.valueOf(msg));
        alert.setTitle("提示");
        alert.showAndWait();
    }

    public static <T> void warn(T msg) {
        javafx.scene.control.Alert alert = new javafx.scene.control.Alert(javafx.scene.control.Alert.AlertType.WARNING);
        alert.setContentText(String.valueOf(msg));
        alert.setTitle("警告");
        alert.showAndWait();
    }
}
