package com.jonny.dbtools.app

import groovy.transform.CompileStatic
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.stage.Stage

@CompileStatic
class MainApplication extends Application {


    static void main(String[] args) {
        launch(args);
    }


    @Override
    void start(Stage stage) {

        def fxmlLoader = new FXMLLoader(MainApplication.class.getResource("main.fxml"))
        def scene = new Scene(fxmlLoader.load() as Parent, 630, 600)
        stage.setTitle("db-tools基础版")
        stage.setScene(scene)
        stage.getScene().getStylesheets().add("org/kordamp/bootstrapfx/bootstrapfx.css");

        // 绑定tabPanel事件
        // MainController controller = fxmlLoader.getController()
        // controller.bindEvent()

        stage.show()

    }

}
