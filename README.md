# Maven JavaFX + Groovy + screw

### 导出html,markdown,doc文档

* 支持mysql、oracle、postgres
* [bootstrap css](https://github.com/kordamp/bootstrapfx?files=1)
* idea安装插件: JavaFX Runtime for plugsins
* scenebuilder在jdk11.0.2+版本自带
* [jdk11下载](https://www.oracle.com/java/technologies/downloads/#java11)

### 界面

![](image/main.png)

### openjdk17启动

```shell
 mvn -s D:\dev_tools\apache-maven-3.6.3\conf\settings.xml javafx:run
```

# QA
* Q1: 提示--module-path问题？
* A: vm添加配置`--module-path "jdk11_path"`或者在idea里面maven编辑指定jdk11目录路径

```shell
--module-path F:\dev_tools\jdk-11.0.13\lib --add-modules javafx.controls,javafx.com.jonny.dbtools.app,javafx.web
--module-path F:\dev_tools\jdk-17.0.1\lib --add-modules javafx.controls,javafx.fxml 
```



* Q2: Java FX Packager: Can't build artifact - fx:deploy is not available in this JDK



* Q3: 通过`<fx:include source="form.xml" fx:id="form"/>`嵌入模板，在另一个`Controller`中通过`@FXML`声明控制器，控制器对象为`null`
* A: 这里的`fx:id="form"`必须和控制器对应，`id`为`form`
  * 控制器必须为`FormController`，如果控制器为`Form1Controller`就为`null`,必须对应起来
  * 一个`fxml`对应一个控制器，不能为同一个，模板文件定义了那些组件，必须在对应的控制器中通过`@FXML`定义出来



* Q4: 想要在初始化的时候绑定事件或做一些操作？
  * 方式1
  ```java
  //在Application类中的start方法中调用
  //绑定tabPanel事件
  MainController controller = fxmlLoader.getController()
  controller.bindEvent()
  ```
  * 方式2【推荐】
  ```java
  //控制器实现Initializable接口，实现initialize方法
  @Override
  void initialize(URL location, ResourceBundle resources) {
        bindEvent();
  }
  ```
  
* Q5: 如何获取屏幕宽度和高度？
* A: 通过
  ```java
  int screenWidth = (int) Screen.getPrimary().getBounds().getWidth();
  int screenHeight = (int) Screen.getPrimary().getBounds().getHeight(); 
  ```
  
* Q6: 如何在一个应用窗口打开另一个应用窗口？
  * A: 调用代码
  ```java
  Platform.runLater(new Runnable() {
      void run() {
            new MainPlusApplication().start(new Stage());
      }
  });
  ```
  
* Q7: 参考
  * https://github.com/openjfx/javafx-maven-plugin


## 鸣谢

* screw